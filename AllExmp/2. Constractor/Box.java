
package classwork;

public class Box 
{
    private int width;
    private int height;
    private int depth;
    
	
    public Box(int width, int height, int depth)
    {
        this.width = width;
        this.height = height;
        this.depth = depth;
    }
    
    public void print()
    {
        System.out.println("Width = " + width + ", Height = " + height + ", Depth = " + depth + ", Volume = " + volume());
    }
    
    public int volume()
    {
        return width * height * depth; 
    }
}


