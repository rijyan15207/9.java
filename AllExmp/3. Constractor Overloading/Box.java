
package classwork;

public class Box 
{
    double width;
    double height;
    double depth;
    
    public Box(int width, int height, int depth)
    {
        this.width = width;
        this.height = height;
        this.depth = depth;
    }
    
    
    public Box(double width, double height, double depth)
    {
        this.width = width;
        this.height = height;
        this.depth = depth;
    }
    
    
    public Box(double width)
    {
        this.width = width;
        this.height = width;
        this.depth = width;
    }
    
    
    public void print()
    {
        System.out.println("Width = " + width + ", Height = " + height + ", Depth = " + depth + ", Volume = " + volume());
    }
    
    public double volume()
    {
        return width * height * depth; 
    }
}
