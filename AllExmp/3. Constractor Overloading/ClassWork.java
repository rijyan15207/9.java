
package classwork;

public class ClassWork 
{

    public static void main(String[] args) 
    {
        Box box1 = new Box(10, 5, 3);
        box1.print();
        
        Box box2 = new Box(7, 4, 1);
        box2.print();
        
        Box box3 = new Box(5.6);
        box3.print();
    }
    
}
